mod annotation;
mod stack_frame;
mod type_annotation;

pub use self::annotation::*;
pub use self::stack_frame::*;
pub use self::type_annotation::*;

use class_file::ClassAccessFlags;

#[derive(Debug, PartialEq)]
pub struct AttributeInfo {
    pub attribute_name_index: u16,
    pub attribute_length: u32,
    pub info: AttributeInfoTypes,
}

impl AttributeInfo {
    pub fn attribute_name_index(&self) -> u16 {
        self.attribute_name_index
    }

    pub fn attribute_info(&self) -> &AttributeInfoTypes {
        &self.info
    }
}

#[derive(Debug, PartialEq)]
pub enum AttributeInfoTypes {
    ConstantValue {
        constantvalue_index: u16,
    },
    Code {
        max_stack: u16,
        max_locals: u16,
        code_length: u32,
        code: Vec<u8>,
        exception_table_length: u16,
        exception_table: Vec<ExceptionData>,
        attributes_count: u16,
        attribute_info: Vec<AttributeInfo>,
    },
    StackMapTable {
        number_of_entries: u16,
        entries: Vec<StackMapFrame>,
    },
    Exceptions {
        number_of_exceptions: u16,
        exception_index_table: Vec<u16>,
    },
    BootstrapMethods {
        num_bootstrap_methods: u16,
        bootstrap_methods: Vec<BootstrapMethodsData>,
    },
    InnerClasses {
        number_of_classes: u16,
        classes: Box<InnerClassData>,
    },
    EnclosingMethod {
        class_index: u16,
        method_index: u16,
    },
    Synthetic,
    Signature {
        signature_index: u16,
    },
    SourceFile {
        source_file_index: u16,
    },
    DebugExtension {
        debug_extension: Vec<u8>,
    },
    LineNumberTable {
        line_number_table_length: u16,
        line_number_table: Vec<LineNumberTableData>,
    },
    LocalVariableTable {
        local_variable_table_length: u16,
        local_variable_table: Vec<LocalVariableTableData>,
    },
    LocalVariableTypeTable {
        local_variable_type_table_length: u16,
        local_variable_table: Vec<LocalVariableTypeTableData>,
    },
    Deprecated,
    RuntimeVisibleAnnotations {
        num_annotations: u16,
        annotations: Vec<Annotation>,
    },
    RuntimeInvisibleAnnotations {
        num_annotations: u16,
        annotations: Vec<Annotation>,
    },
    RuntimeVisibleParameterAnnotations {
        num_parameters: u8,
        parameter_annotations: Vec<(u16, Box<Annotation>)>,
    },
    RuntimeInvisibleParameterAnnotations {
        num_parameters: u8,
        parameter_annotations: Box<(u16, Box<Annotation>)>,
    },
    RuntimeVisibleTypeAnnotations {
        // TODO
        num_annotations: u16,
        annotations: Vec<TypeAnnotation>,
    },
    RuntimeInvisibleTypeAnnotations, //TODO
    MethodParameters {
        parameters_count: u16,
        parameters: Vec<MethodParametersData>,
    },
    Custom {
        info: Vec<u8>,
    },
}

bitflags! {
    pub struct MethodParametersAccessFlags : u16 {
        const ACC_FINAL = 0x0010;
        const ACC_SYNTHETIC = 0x1000;
        const ACC_MANDATED = 0x8000;
    }
}

#[derive(Debug, PartialEq)]
pub struct MethodParametersData {
    pub num_index: u16,
    pub access_flags: MethodParametersAccessFlags,
}

#[derive(Debug, PartialEq)]
pub struct BootstrapMethodsData {
    pub bootstrap_method_ref: u16,
    pub num_bootstrap_arguments: u16,
    pub bootstrap_arguments: Vec<u16>,
}

#[derive(Debug, PartialEq)]
pub struct LocalVariableTypeTableData {
    pub start_pc: u16,
    pub length: u16,
    pub name_index: u16,
    pub signature_index: u16,
    pub index: u16,
}

#[derive(Debug, PartialEq)]
pub struct LocalVariableTableData {
    pub start_pc: u16,
    pub length: u16,
    pub name_index: u16,
    pub descriptor_index: u16,
    pub index: u16,
}

#[derive(Debug, PartialEq)]
pub struct LineNumberTableData {
    pub start_pc: u16,
    pub line_number: u16,
}

#[derive(Debug, PartialEq)]
pub struct InnerClassData {
    pub inner_class_info_index: u16,
    pub outer_class_info_index: u16,
    pub inner_name_index: u16,
    pub inner_class_access_flags: ClassAccessFlags,
}

#[derive(Debug, PartialEq)]
pub struct ExceptionData {
    pub start_pc: u16,
    pub end_pc: u16,
    pub handler_pc: u16,
    pub catch_type: u16,
}
