#[derive(Debug, PartialEq)]
pub enum StackMapFrameData {
    SameFrame,
    SameLocalsOneStackItemFrame {
        stack: VerificationTypeInfo,
    },
    SameLocalsOneStackItemFrameExtended {
        offset_delta: u16,
        stack: VerificationTypeInfo,
    },
    ChopFrame {
        offset_delta: u16,
    },
    SameFrameExtended {
        offset_delta: u16,
    },
    AppendFrame {
        offset_delta: u16,
        locals: Vec<VerificationTypeInfo>,
    },
    FullFrame {
        offset_delta: u16,
        number_of_locals: u16,
        locals: Vec<VerificationTypeInfo>,
        number_of_stack_items: u16,
        stack: Vec<VerificationTypeInfo>,
    },
}

#[derive(Debug, PartialEq)]
pub struct StackMapFrame {
    tag: u8,
    stack_map: StackMapFrameData,
}

impl StackMapFrame {
    pub fn same_frame(tag: u8) -> Self {
        assert!(tag <= 63);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::SameFrame,
        }
    }

    pub fn same_locals_one_stack_item_frame(tag: u8, stack: VerificationTypeInfo) -> Self {
        assert!(tag >= 64 && tag <= 127);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::SameLocalsOneStackItemFrame { stack },
        }
    }

    pub fn same_locals_one_stack_item_frame_extended(
        tag: u8,
        offset_delta: u16,
        stack: VerificationTypeInfo,
    ) -> Self {
        assert!(tag == 247);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::SameLocalsOneStackItemFrameExtended {
                offset_delta,
                stack,
            },
        }
    }

    pub fn chop_frame(tag: u8, offset_delta: u16) -> Self {
        assert!(tag >= 248 && tag <= 250);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::ChopFrame { offset_delta },
        }
    }

    pub fn same_frame_extended(tag: u8, offset_delta: u16) -> Self {
        assert!(tag == 251);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::SameFrameExtended { offset_delta },
        }
    }

    pub fn append_frame(tag: u8, offset_delta: u16, locals: Vec<VerificationTypeInfo>) -> Self {
        assert!(tag >= 252 && tag <= 254);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::AppendFrame {
                offset_delta,
                locals,
            },
        }
    }

    pub fn full_frame(
        tag: u8,
        offset_delta: u16,
        locals: Vec<VerificationTypeInfo>,
        stack: Vec<VerificationTypeInfo>,
    ) -> Self {
        assert!(tag == 255);
        StackMapFrame {
            tag,
            stack_map: StackMapFrameData::FullFrame {
                offset_delta,
                number_of_locals: locals.len() as u16,
                locals,
                number_of_stack_items: stack.len() as u16,
                stack,
            },
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct VerificationTypeInfo {
    tag: u8,
    data: VerificationTypeInfoData,
}

impl VerificationTypeInfo {
    pub fn top_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 0,
            data: VerificationTypeInfoData::TopVariableInfo,
        }
    }

    pub fn integer_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 1,
            data: VerificationTypeInfoData::IntegerVariableInfo,
        }
    }

    pub fn float_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 2,
            data: VerificationTypeInfoData::FloatVariableInfo,
        }
    }

    pub fn long_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 3,
            data: VerificationTypeInfoData::LongVariableInfo,
        }
    }

    pub fn double_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 4,
            data: VerificationTypeInfoData::DoubleVariableInfo,
        }
    }

    pub fn null_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 5,
            data: VerificationTypeInfoData::NullVariableInfo,
        }
    }

    pub fn unitialized_this_variable_info() -> Self {
        VerificationTypeInfo {
            tag: 6,
            data: VerificationTypeInfoData::UnitializedThisVariableInfo,
        }
    }

    pub fn object_variable_info(constant_pool_index: u16) -> Self {
        VerificationTypeInfo {
            tag: 7,
            data: VerificationTypeInfoData::ObjectVariableInfo {
                constant_pool_index,
            },
        }
    }

    pub fn unitialized_variable_info(offset: u16) -> Self {
        VerificationTypeInfo {
            tag: 8,
            data: VerificationTypeInfoData::UnitializedVariableInfo { offset },
        }
    }
}

// TODO: The solution using this enum does not properly follows the spec.
// The size of this enum will be 2 bytes for any possible enum data, which will
// make most VerificationTypeInfo instances be bigger than they should be.
#[derive(Debug, PartialEq)]
pub enum VerificationTypeInfoData {
    TopVariableInfo,
    IntegerVariableInfo,
    FloatVariableInfo,
    LongVariableInfo,
    DoubleVariableInfo,
    NullVariableInfo,
    UnitializedThisVariableInfo,
    ObjectVariableInfo { constant_pool_index: u16 },
    UnitializedVariableInfo { offset: u16 },
}
