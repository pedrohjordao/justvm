use super::annotation::ElementValue;

#[derive(Debug, PartialEq)]
pub struct TypeAnnotation {
    pub target_type: u8,
    pub target_info: TargetInfoData,
    pub target_path: TypePath,
    type_index: u16,
    num_element_value_pairs: u16,
    element_value_pairs: Box<[(u16, ElementValue)]>,
}

#[derive(Debug, PartialEq)]
pub enum TargetInfoData {}

#[derive(Debug, PartialEq)]
pub struct TypePath {}
