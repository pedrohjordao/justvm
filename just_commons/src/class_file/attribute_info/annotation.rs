#[derive(Debug, PartialEq)]
pub struct Annotation {
    pub type_index: u16,
    pub num_element_value_paris: u16,
    pub element_value_paris: Box<[(u16, ElementValue)]>,
}

#[derive(Debug, PartialEq)]
pub struct ElementValue {
    pub tag: u8, //TODO: char? A new enum?
    pub values: ElementValueTypes,
}

#[derive(Debug, PartialEq)]
pub enum ElementValueTypes {
    ConstantValueIndex {
        index: u16,
    },
    EnumConstvalue {
        type_name_index: u16,
        const_name_index: u16,
    },
    ClassInfoIndex {
        index: u16,
    },
    AnnotationValue {
        annotation: Annotation,
    },
    ArrayValue {
        num_values: u16,
        values: Box<[ElementValueTypes]>,
    },
}
