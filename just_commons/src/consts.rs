pub static MAGIC_NUMBER: [u8; 4] = [0xCA, 0xFE, 0xBA, 0xBE];

pub static JAVA_1_5_VERSION: u32 = 49;
pub static JAVA_6_VERSION: u32 = 50;
pub static JAVA_7_VERSION: u32 = 51;
pub static JAVA_8_VERSION: u32 = 52;
