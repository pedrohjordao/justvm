use std::marker::PhantomData;
use std::rc::Rc;

pub enum ValueType {
    ClassRef,
    Array,
    InterfaceRef,
    Byte,
    Short,
    Integer,
    Long,
    Char,
    Boolean,
    Float,
    Double,
    ReturnAddress,
}

pub trait JPrimitive {}
pub trait JReference {
    fn is_null(&self) -> bool;
}
pub trait JIntegral: JPrimitive {}
pub trait JFloatingPoint: JPrimitive {}

pub trait JValue {
    const VALUE_TYPE: ValueType;
    type DataRepresentation;

    fn size_in_bytes() -> usize {
        use std::mem;
        mem::size_of::<Self::DataRepresentation>()
    }

    fn get_value(&self) -> &Self::DataRepresentation;
}

#[derive(Debug)]
pub struct JByte(i8);

impl JPrimitive for JByte {}

impl JIntegral for JByte {}

impl Default for JByte {
    fn default() -> Self {
        JByte(0)
    }
}

impl JValue for JByte {
    const VALUE_TYPE: ValueType = ValueType::Byte;
    type DataRepresentation = i8;

    fn get_value(&self) -> &Self::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JShort(i16);

impl JPrimitive for JShort {}

impl JIntegral for JShort {}

impl Default for JShort {
    fn default() -> Self {
        JShort(0)
    }
}

impl JValue for JShort {
    const VALUE_TYPE: ValueType = ValueType::Short;
    type DataRepresentation = i16;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JInteger(i32);

impl JPrimitive for JInteger {}

impl JIntegral for JInteger {}

impl Default for JInteger {
    fn default() -> Self {
        JInteger(0)
    }
}

impl JValue for JInteger {
    const VALUE_TYPE: ValueType = ValueType::Integer;
    type DataRepresentation = i32;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JLong(i64);

impl JPrimitive for JLong {}

impl JIntegral for JLong {}

impl Default for JLong {
    fn default() -> Self {
        JLong(0)
    }
}

impl JValue for JLong {
    const VALUE_TYPE: ValueType = ValueType::Long;
    type DataRepresentation = i64;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JChar(u16);

impl JPrimitive for JChar {}

impl JIntegral for JChar {}

impl Default for JChar {
    fn default() -> Self {
        JChar(0)
    }
}

impl JValue for JChar {
    const VALUE_TYPE: ValueType = ValueType::Char;
    type DataRepresentation = u16;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JFloat(f32);

impl JPrimitive for JFloat {}

impl JFloatingPoint for JFloat {}

impl Default for JFloat {
    fn default() -> Self {
        JFloat(0.0)
    }
}

impl JValue for JFloat {
    const VALUE_TYPE: ValueType = ValueType::Float;
    type DataRepresentation = f32;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JDouble(f64);

impl JPrimitive for JDouble {}

impl JFloatingPoint for JDouble {}

impl Default for JDouble {
    fn default() -> Self {
        JDouble(0.0)
    }
}

impl JValue for JDouble {
    const VALUE_TYPE: ValueType = ValueType::Double;
    type DataRepresentation = f64;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JBoolean(i8);

impl JPrimitive for JBoolean {}

impl Default for JBoolean {
    fn default() -> Self {
        JBoolean(1)
    }
}

impl JValue for JBoolean {
    const VALUE_TYPE: ValueType = ValueType::Boolean;
    type DataRepresentation = i8;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct OPCode;

#[derive(Debug)]
pub struct JReturnAddress(Rc<OPCode>);

impl JPrimitive for JReturnAddress {}

impl JValue for JReturnAddress {
    const VALUE_TYPE: ValueType = ValueType::ReturnAddress;
    type DataRepresentation = Rc<OPCode>;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.0
    }
}

#[derive(Debug)]
pub struct JClass;

impl JReference for JClass {
    fn is_null(&self) -> bool {
        unimplemented!()
    }
}

impl JValue for JClass {
    const VALUE_TYPE: ValueType = ValueType::ClassRef;
    type DataRepresentation = ();

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &()
    }
}

#[derive(Debug)]
pub struct JInterface;

impl JReference for JInterface {
    fn is_null(&self) -> bool {
        unimplemented!()
    }
}

impl JValue for JInterface {
    const VALUE_TYPE: ValueType = ValueType::InterfaceRef;
    type DataRepresentation = ();

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &()
    }
}

#[derive(Debug)]
pub struct JArray<DataType: JReference, ElementType: JReference> {
    data: Vec<DataType>,
    _dt: PhantomData<DataType>,
    _et: PhantomData<ElementType>,
}

impl<T: JReference, W: JReference> JReference for JArray<T, W> {
    fn is_null(&self) -> bool {
        unimplemented!()
    }
}

impl<T: JReference, W: JReference> JValue for JArray<T, W> {
    const VALUE_TYPE: ValueType = ValueType::Array;
    type DataRepresentation = Vec<T>;

    fn get_value(&self) -> &<Self as JValue>::DataRepresentation {
        &self.data
    }
}
