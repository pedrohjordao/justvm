extern crate just_class_reader;
extern crate just_commons;

use just_class_reader::class_file_parser;
use just_commons::class_file::{
    ClassAccessFlags, ClassFile, const_info::ConstantInfo, method_info::*,
};

#[test]
fn class_file_parser_test() {
    let data = include_bytes!("HelloWorld.class");
    let parsed = class_file_parser(data);
    assert!(
        parsed.is_ok(),
        format!(
            "Was not able to parse verification_type_info::unitialized_variable_info: {:?}",
            parsed
        )
    );
    let (remaining, parsed) = parsed.unwrap();

    assert_eq!(parsed.minor_version, 0, "Read wrong minor version");
    assert_eq!(parsed.major_version, 52, "Read wrong major version");
    assert_eq!(
        parsed.access_flags,
        ClassAccessFlags::ACC_SUPER | ClassAccessFlags::ACC_PUBLIC,
        "Incorrectly read access flags from class_file"
    );
    //the count is larger than the len by one according to the standard
    assert_eq!(
        parsed.const_pool_count, 29,
        "Parsed constant pool count wrongly"
    );
    assert_eq!(
        parsed.constant_pool.len(),
        28,
        "Parsed wrong number of constant pool itens"
    );

    assert_eq!(parsed.this_class, 5);
    // this index is always off by one
    assert_eq!(
        parsed.constant_pool[5 - 1],
        ConstantInfo::ClassInfo { name_index: 21 }
    );
    assert_eq!(
        parsed.constant_pool[21 - 1],
        ConstantInfo::Utf8Info {
            data: "HelloWorld".into()
        }
    );

    assert_eq!(parsed.super_class, 6);
    assert_eq!(
        parsed.constant_pool[6 - 1],
        ConstantInfo::ClassInfo { name_index: 22 }
    );
    assert_eq!(
        parsed.constant_pool[22 - 1],
        ConstantInfo::Utf8Info {
            data: "java/lang/Object".into()
        }
    );

    assert_eq!(parsed.interfaces_count, 0);
    assert!(parsed.interfaces.is_empty());

    assert_eq!(parsed.fields_count, 0);
    assert!(parsed.fields.is_empty());

    assert_eq!(parsed.methods_count, 2);
    assert_eq!(parsed.methods[1 - 1].attribute_count, 1);
    assert_eq!(parsed.methods[2 - 1].attribute_count, 1);

    assert_eq!(remaining.len(), 0, "Classfile was not parsed to the end");
}
