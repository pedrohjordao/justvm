extern crate byteorder;
extern crate just_commons;
#[macro_use]
extern crate nom;

use byteorder::{BigEndian, ReadBytesExt};
use nom::{be_f32, be_f64, be_i32, be_i64, be_u16, be_u32, be_u8, IResult};

use just_commons::class_file::{ClassAccessFlags, ClassFile};
use just_commons::class_file::attribute_info::*;
use just_commons::class_file::const_info::*;
use just_commons::class_file::field_info::*;
use just_commons::class_file::method_info::*;

named!(magic_parser<&[u8], u32>, map_res!(tag!(&just_commons::consts::MAGIC_NUMBER), |mut bytes: &[u8]| bytes.read_u32::<BigEndian>())) ;

fn const_info_parser<'a>(input: &'a [u8]) -> IResult<&'a [u8], ConstantInfo> {
    let (input, tag) = be_u8(input)?;

    match DataType::from_tag(tag) {
        DataType::Class => map!(input, be_u16, |name_index| ConstantInfo::ClassInfo {
            name_index
        }),
        DataType::FieldRef => map!(input, tuple!(be_u16, be_u16), |(
            class_index,
            name_and_type_index,
        )| {
            ConstantInfo::FieldRefInfo {
                class_index,
                name_and_type_index,
            }
        }),
        DataType::MethodRef => map!(input, tuple!(be_u16, be_u16), |(
            class_index,
            name_and_type_index,
        )| {
            ConstantInfo::MethodRefInfo {
                class_index,
                name_and_type_index,
            }
        }),
        DataType::InterfaceMethodRef => map!(input, tuple!(be_u16, be_u16), |(
            class_index,
            name_and_type_index,
        )| {
            ConstantInfo::InterfaceMethodRefInfo {
                class_index,
                name_and_type_index,
            }
        }),
        DataType::String => map!(input, be_u16, |string_index| ConstantInfo::StringInfo {
            string_index
        }),
        DataType::Integer => map!(input, be_i32, |bytes| ConstantInfo::IntegerInfo { bytes }),
        DataType::Float => map!(input, be_f32, |bytes| ConstantInfo::FloatInfo { bytes }),
        DataType::Long => map!(input, be_i64, |bytes| ConstantInfo::LongInfo { bytes }),
        DataType::Double => map!(input, be_f64, |bytes| ConstantInfo::DoubleInfo { bytes }),
        DataType::NameAndType => map!(input, tuple!(be_u16, be_u16), |(
            name_index,
            descriptor_index,
        )| {
            ConstantInfo::NameAndTypeInfo {
                name_index,
                descriptor_index,
            }
        }),
        DataType::UTF8 => do_parse!(
            input,
            length: be_u16
                >> bytes: take!(length as usize)
                >> (ConstantInfo::Utf8Info {
                    data: String::from(String::from_utf8_lossy(bytes))
                })
        ),
        DataType::MethodHandle => map!(input, tuple!(be_u8, be_u16), |(
            reference_kind,
            reference_index,
        )| {
            ConstantInfo::MethodHandleInfo {
                reference_kind: MethodHandleReferenceKind::from_tag(reference_kind),
                reference_index,
            }
        }),
        DataType::MethodType => map!(input, be_u16, |descriptor_index| {
            ConstantInfo::MethodTypeInfo { descriptor_index }
        }),
        DataType::InvokeDynamic => map!(input, tuple!(be_u16, be_u16), |(
            bootstrap_method_attr_index,
            name_and_type_index,
        )| {
            ConstantInfo::InvokeDynamicInfo {
                bootstrap_method_attr_index,
                name_and_type_index,
            }
        }),
    }
}

fn field_info_parse<'a>(input: &'a [u8], cp: &Vec<ConstantInfo>) -> IResult<&'a [u8], FieldInfo> {
    closure!(
        &'a [u8],
        do_parse!(
            access_flags:
                map_res!(be_u16, |flags| FieldAccessFlags::from_bits(flags)
                    .ok_or(format!("Invalid field access flags: {:b}", flags)))
                >> name_index: be_u16
                >> descriptor_index: be_u16
                >> attribute_count: be_u16
                >> attribute_info:
                    count!(apply!(attribute_info_parse, cp), attribute_count as usize)
                >> (FieldInfo {
                    access_flags,
                    name_index,
                    descriptor_index,
                    attribute_count,
                    attribute_info
                })
        )
    )(input)
}

fn method_info_parse<'a>(input: &'a [u8], cp: &Vec<ConstantInfo>) -> IResult<&'a [u8], MethodInfo> {
    closure!(
        &'a [u8],
        do_parse!(
            access_flags:
                map_res!(be_u16, |flags| MethodAccessFlags::from_bits(flags)
                    .ok_or(format!("Invalid method access flags: {:b}", flags)))
                >> name_index: be_u16
                >> descriptor_index: be_u16
                >> attribute_count: be_u16
                >> attribute_info:
                    count!(apply!(attribute_info_parse, cp), attribute_count as usize)
                >> (MethodInfo {
                    access_flags,
                    name_index,
                    descriptor_index,
                    attribute_count,
                    attribute_info
                })
        )
    )(input)
}

fn attribute_info_parse<'a>(
    input: &'a [u8],
    cp: &Vec<ConstantInfo>,
) -> IResult<&'a [u8], AttributeInfo> {
    let (input, attribute_name_index) = be_u16(input)?;
    let (input, attribute_length) = be_u32(input)?;

    // TODO: Probably better to wrap Vec<ConstInfo> into a new type
    // that deals with the offset internally
    let attribute_name = match cp[(attribute_name_index - 1) as usize] {
        ConstantInfo::Utf8Info { ref data } => data,
        ref x => panic!(
            "Found invalid type at index {}: {:?}",
            attribute_name_index, x
        ),
    };

    match attribute_name.as_ref() {
        "ConstantValue" => map!(input, be_u16, |constantvalue_index| AttributeInfo {
            attribute_name_index,
            attribute_length,
            info: AttributeInfoTypes::ConstantValue {
                constantvalue_index
            }
        }),
        "Code" => do_parse!(
            input,
            max_stack: be_u16
                >> max_locals: be_u16
                >> code_length: be_u32
                >> code: count!(be_u8, code_length as usize)
                >> exception_table_length: be_u16
                >> exception_table: count!(exception_data_parse, exception_table_length as usize)
                >> attributes_count: be_u16
                >> attribute_info:
                    count!(apply!(attribute_info_parse, cp), attributes_count as usize)
                >> (AttributeInfo {
                    attribute_name_index,
                    attribute_length,
                    info: AttributeInfoTypes::Code {
                        max_stack,
                        max_locals,
                        code_length,
                        code,
                        exception_table_length,
                        exception_table,
                        attributes_count,
                        attribute_info
                    }
                })
        ),
        "StackMapTable" => do_parse!(
            input,
            number_of_entries: be_u16
                >> entries: count!(stack_map_frame_parse, number_of_entries as usize)
                >> (AttributeInfo {
                    attribute_name_index,
                    attribute_length,
                    info: AttributeInfoTypes::StackMapTable {
                        number_of_entries,
                        entries
                    }
                })
        ),
        "Exceptions" => do_parse!(
            input,
            number_of_exceptions: be_u16
                >> exception_index_table: count!(be_u16, number_of_exceptions as usize)
                >> (AttributeInfo {
                    attribute_name_index,
                    attribute_length,
                    info: AttributeInfoTypes::Exceptions {
                        number_of_exceptions,
                        exception_index_table
                    }
                })
        ),
        "BootstrapMethods" => do_parse!(
            input,
            num_bootstrap_methods: be_u16
                >> bootstrap_methods:
                    count!(bootstrap_method_data_parse, num_bootstrap_methods as usize)
                >> (AttributeInfo {
                    attribute_name_index,
                    attribute_length,
                    info: AttributeInfoTypes::BootstrapMethods {
                        num_bootstrap_methods,
                        bootstrap_methods
                    }
                })
        ),
        //TODO: Implement the remaining attributes
        _x => do_parse!(
            input,
            info: count!(be_u8, attribute_length as usize)
                >> (AttributeInfo {
                    attribute_name_index,
                    attribute_length,
                    info: AttributeInfoTypes::Custom { info }
                })
        ),
    }
}

named!(bootstrap_method_data_parse<&[u8], BootstrapMethodsData>, do_parse!(
    bootstrap_method_ref: be_u16 >>
    num_bootstrap_arguments: be_u16 >>
    bootstrap_arguments: count!(be_u16, num_bootstrap_arguments as usize) >>
    (
        BootstrapMethodsData {
            bootstrap_method_ref,
            num_bootstrap_arguments,
            bootstrap_arguments
        }
    )
));

fn stack_map_frame_parse<'a>(input: &'a [u8]) -> IResult<&'a [u8], StackMapFrame> {
    let (input, tag) = be_u8(input)?;

    match tag {
        x if x <= 63 => Ok((input, StackMapFrame::same_frame(x))),
        x if x >= 64 && x <= 127 => {
            let (input, stack) = verification_type_info_parse(input)?;
            Ok((
                input,
                StackMapFrame::same_locals_one_stack_item_frame(x, stack),
            ))
        }
        x if x == 247 => {
            let (input, offset_delta) = be_u16(input)?;
            let (input, stack) = verification_type_info_parse(input)?;
            Ok((
                input,
                StackMapFrame::same_locals_one_stack_item_frame_extended(x, offset_delta, stack),
            ))
        }
        x if x >= 248 && x <= 250 => {
            let (input, offset_delta) = be_u16(input)?;
            Ok((input, StackMapFrame::chop_frame(x, offset_delta)))
        }
        x if x == 251 => {
            let (input, offset_delta) = be_u16(input)?;
            Ok((input, StackMapFrame::same_frame_extended(x, offset_delta)))
        }
        x if x >= 252 && x <= 254 => {
            let (input, offset_delta) = be_u16(input)?;
            let (input, locals) = count!(input, verification_type_info_parse, (x - 251) as usize)?;
            Ok((input, StackMapFrame::append_frame(x, offset_delta, locals)))
        }
        x if x == 255 => {
            let (input, offset_delta) = be_u16(input)?;
            let (input, number_of_locals) = be_u16(input)?;
            let (input, locals) = count!(
                input,
                verification_type_info_parse,
                number_of_locals as usize
            )?;
            let (input, number_of_stack_itens) = be_u16(input)?;
            let (input, stack) = count!(
                input,
                verification_type_info_parse,
                number_of_stack_itens as usize
            )?;
            Ok((
                input,
                StackMapFrame::full_frame(x, offset_delta, locals, stack),
            ))
        }
        // TODO: Return error instead of panic
        x => panic!(format!("Invalid tag for stack_map_frame: {}", x)),
    }
}

fn verification_type_info_parse<'a>(input: &'a [u8]) -> IResult<&'a [u8], VerificationTypeInfo> {
    let (input, tag) = be_u8(input)?;

    match tag {
        x if x == 0 => Ok((input, VerificationTypeInfo::top_variable_info())),
        x if x == 1 => Ok((input, VerificationTypeInfo::integer_variable_info())),
        x if x == 2 => Ok((input, VerificationTypeInfo::float_variable_info())),
        x if x == 3 => Ok((input, VerificationTypeInfo::long_variable_info())),
        x if x == 4 => Ok((input, VerificationTypeInfo::double_variable_info())),
        x if x == 5 => Ok((input, VerificationTypeInfo::null_variable_info())),
        x if x == 6 => Ok((
            input,
            VerificationTypeInfo::unitialized_this_variable_info(),
        )),
        x if x == 7 => {
            let (input, constant_pool_index) = be_u16(input)?;
            Ok((
                input,
                VerificationTypeInfo::object_variable_info(constant_pool_index),
            ))
        }
        x if x == 8 => {
            let (input, offset) = be_u16(input)?;
            Ok((
                input,
                VerificationTypeInfo::unitialized_variable_info(offset),
            ))
        }
        //TODO: Use an error instead of panic
        x => panic!("Invalid variable_info tag: {}", x),
    }
}

named!(exception_data_parse<&[u8], ExceptionData>,
    do_parse!(
        start_pc: be_u16 >>
        end_pc: be_u16 >>
        handler_pc: be_u16 >>
        catch_type: be_u16 >>
        (
            ExceptionData {
                start_pc,
                end_pc,
                handler_pc,
                catch_type
            }
        )
    )
);

named_attr!(#[doc = "Parses a byte array as a ClassFile"],
pub class_file_parser<&[u8], ClassFile>,
    do_parse!(
        magic: magic_parser >>
        minor_version: be_u16 >>
        major_version: be_u16 >>
        const_pool_count: be_u16 >>
        constant_pool: count!(const_info_parser, (const_pool_count - 1) as usize) >> 
        access_flags: map_res!(be_u16, |flags| ClassAccessFlags::from_bits(flags).ok_or(format!("Invalid class access flags: {:b}", flags))) >>
        this_class: be_u16 >>
        super_class: be_u16 >>
        interfaces_count: be_u16 >>
        interfaces: count!(be_u16, interfaces_count as usize) >>
        fields_count: be_u16 >>
        fields: count!(apply!(field_info_parse, &constant_pool), fields_count as usize) >>
        methods_count: be_u16 >>
        methods: count!(apply!(method_info_parse, &constant_pool), methods_count as usize) >>
        attributes_count: be_u16 >>
        attributes: count!(apply!(attribute_info_parse, &constant_pool), attributes_count as usize) >>
        (
            ClassFile {
                magic, 
                minor_version,
                major_version,
                const_pool_count,
                constant_pool,
                access_flags,
                this_class,
                super_class,
                interfaces_count,
                interfaces,
                fields_count,
                fields,
                methods_count,
                methods,
                attributes_count,
                attributes
            }
        )
    )
);

#[cfg(test)]
mod test {
    use nom::IResult;

    use super::*;

    #[test]
    fn parse_magic() {
        let data = [0xCA, 0xFE, 0xBA, 0xBE, 0xBE, 0xEF];
        let parsed = magic_parser(&data);
        assert!(parsed.is_ok());
        let (remaining, _) = parsed.unwrap();
        assert_eq!(remaining, &[0xBE, 0xEF]);
        let data = [0xDE, 0xAD, 0xBE, 0xEF];
        assert!(magic_parser(&data).is_err());
    }

    #[test]
    fn const_info_parse_class() {
        //          class tag | name index | remaining data
        let data = [7u8, 0u8, 15u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse class_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, ConstantInfo::ClassInfo { name_index: 15 });
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for class_info"
        );
    }

    #[test]
    fn const_info_parse_field_ref() {
        //          class tag | class_index | name_and_type_index | remaining data
        let data = [9u8, 0u8, 15u8, 0u8, 16u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse field_ref_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::FieldRefInfo {
                class_index: 15,
                name_and_type_index: 16
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for field_ref_info"
        );
    }

    #[test]
    fn const_info_parse_method_ref() {
        //          class tag | class_index | name_and_type_index | remaining data
        let data = [10u8, 0u8, 15u8, 0u8, 16u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse method_ref_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::MethodRefInfo {
                class_index: 15,
                name_and_type_index: 16
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for method_ref_info"
        );
    }

    #[test]
    fn const_info_parse_interface_method_ref() {
        //          class tag | class_index | name_and_type_index | remaining data
        let data = [11u8, 0u8, 15u8, 0u8, 16u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse interface_method_ref_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::InterfaceMethodRefInfo {
                class_index: 15,
                name_and_type_index: 16
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for interface_method_ref_info"
        );
    }

    #[test]
    fn const_info_parse_string() {
        //          class tag | string index | remaining data
        let data = [8u8, 0u8, 15u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse string_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, ConstantInfo::StringInfo { string_index: 15 });
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for string_info"
        );
    }

    #[test]
    fn const_info_parse_integer() {
        //          class tag |      i32 bytes     | remaining data
        let data = [3u8, 0u8, 0u8, 0u8, 15u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse integer_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, ConstantInfo::IntegerInfo { bytes: 15 });
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for integer_info"
        );
    }

    #[test]
    fn const_info_parse_long() {
        //          class tag |                i64 bytes                | remaining data
        let data = [
            5u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 15u8, 5u8, 10u8, 3u8, 3u8,
        ];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse long_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, ConstantInfo::LongInfo { bytes: 15 });
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for long_info"
        );
    }

    #[test]
    fn const_info_parse_float() {
        //          class tag |       f32 bytes      | remaining data
        let data = [4u8, 65u8, 112u8, 0u8, 0u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse float_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, ConstantInfo::FloatInfo { bytes: 15. });
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for float_info"
        );
    }

    #[test]
    fn const_info_parse_double() {
        //          class tag |                f64 bytes                 | remaining data
        let data = [
            6u8, 64u8, 46u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 5u8, 10u8, 3u8, 3u8,
        ];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse double_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, ConstantInfo::DoubleInfo { bytes: 15. });
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for double_info"
        );
    }

    #[test]
    fn const_info_parse_name_and_type() {
        //          class tag | name_index | descriptor_index | remaining data
        let data = [12u8, 0u8, 46u8, 0u8, 15u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse name_and_type_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::NameAndTypeInfo {
                name_index: 46,
                descriptor_index: 15
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for name_and_type_info"
        );
    }

    #[test]
    fn const_info_parse_utf8() {
        //TODO: Way more testing is  needed for utf8
        //          class tag |  length   |            data             | remaining data
        let data = [
            1u8, 0u8, 5u8, b'H', b'e', b'l', b'l', b'o', 5u8, 10u8, 3u8, 3u8,
        ];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse utf8_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::Utf8Info {
                data: "Hello".into()
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for utf8_info"
        );
    }

    #[test]
    fn const_info_parse_method_handle() {
        //          class tag | reference_kind | reference_index | remaining data
        let data = [15u8, 1u8, 0u8, 5u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse method_handle_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::MethodHandleInfo {
                reference_kind: MethodHandleReferenceKind::RefGetField,
                reference_index: 5
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for method_handle_info"
        );
    }

    #[test]
    fn const_info_parse_method_type() {
        //          class tag | descriptor_index | remaining data
        let data = [16u8, 0u8, 5u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse method_type_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::MethodTypeInfo {
                descriptor_index: 5
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for method_type_info"
        );
    }

    #[test]
    fn const_info_parse_invoke_dynamic() {
        //          class tag | bootstrap_method_attr_index | name_and_type_index | remaining data
        let data = [18u8, 0u8, 5u8, 0u8, 10u8, 5u8, 10u8, 3u8, 3u8];
        let parsed = const_info_parser(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse invoke_dynamic_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ConstantInfo::InvokeDynamicInfo {
                bootstrap_method_attr_index: 5,
                name_and_type_index: 10
            }
        );
        assert_eq!(
            remaining,
            [5u8, 10u8, 3u8, 3u8],
            "Parsed too much data for invoke_dynamic_info"
        );
    }

    #[test]
    fn attribute_info_parse_const_value() {
        let cp = vec![ConstantInfo::Utf8Info {
            data: "ConstantValue".into(),
        }];
        //          attribute_name_index | attribute_length  | constantvalue_index | remaining
        let data = [0u8, 1u8, 0u8, 0u8, 0u8, 2u8, 0u8, 2u8, 42u8, 8u8];

        let parsed = attribute_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse ConstantValue_attribute: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            AttributeInfo {
                attribute_name_index: 1,
                attribute_length: 2,
                info: AttributeInfoTypes::ConstantValue {
                    constantvalue_index: 2
                }
            }
        );
        assert_eq!(
            remaining,
            [42u8, 8u8],
            "Parsed too much data for ConstantValue_attribute"
        );
    }

    //TODO: test all (or at least most) variations using field_info
    #[test]
    fn field_info_parse_const_value_public_final() {
        let cp = vec![
            ConstantInfo::Utf8Info {
                data: "ConstantValue".into(),
            },
            ConstantInfo::Utf8Info { data: "foo".into() },
        ];
        //         |      access_flags     | name_index | descriptor_index | attribute_count
        let data = [
            0b00000000, 0b00010001, 0u8, 2u8, 0u8, 3u8, 0u8, 1u8,
            //         | attribute_info for constvalue
            0u8, 1u8, 0u8, 0u8, 0u8, 2u8, 0u8, 2u8, //         | remaining
            42u8, 8u8,
        ];
        let parsed = field_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse field_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            FieldInfo {
                access_flags: FieldAccessFlags::ACC_PUBLIC | FieldAccessFlags::ACC_FINAL,
                name_index: 2,
                descriptor_index: 3,
                attribute_count: 1,
                attribute_info: vec![AttributeInfo {
                    attribute_name_index: 1,
                    attribute_length: 2,
                    info: AttributeInfoTypes::ConstantValue {
                        constantvalue_index: 2
                    }
                }]
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much data");
    }

    //TODO: test all (or at least most) variations using method_info
    #[test]
    fn method_info_parse_public_final_synchronized() {
        let cp = vec![
            ConstantInfo::Utf8Info {
                data: "Exceptions".into(),
            },
            ConstantInfo::Utf8Info { data: "foo".into() },
        ];
        //         |      access_flags     | name_index | descriptor_index | attribute_count
        let data = [
            0b00000000, 0b00110001, 0u8, 2u8, 0u8, 3u8, 0u8, 1u8,
            //         | attribute_info for exceptions
            0u8, 1u8, 0u8, 0u8, 0u8, 4u8, 0u8, 1u8, 0u8, 1u8, //         | remaining
            42u8, 8u8,
        ];
        let parsed = method_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse method_info: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            MethodInfo {
                access_flags: MethodAccessFlags::ACC_PUBLIC
                    | MethodAccessFlags::ACC_FINAL
                    | MethodAccessFlags::ACC_SYNCHRONIZED,
                name_index: 2,
                descriptor_index: 3,
                attribute_count: 1,
                attribute_info: vec![AttributeInfo {
                    attribute_name_index: 1,
                    attribute_length: 4,
                    info: AttributeInfoTypes::Exceptions {
                        number_of_exceptions: 1,
                        exception_index_table: vec![1]
                    }
                }]
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much data");
    }

    #[test]
    fn attribute_info_parse_code() {
        let cp = vec![ConstantInfo::Utf8Info {
            data: "Code".into(),
        }];

        // Right now attribute_length is ignored
        //         attribute_name_index |  attribute_length  | max_stack | max_locals |   code_length     |
        let data = [
            0u8, 1u8, 0u8, 0u8, 0u8, 0u8, 0u8, 10u8, 0u8, 10u8, 0u8, 0u8, 0u8, 5u8,
            //         |           code          | exception_table_length | start_pc |  end_pc  | handler_pc | catch_type |
            0u8, 108u8, 5u8, 10u8, 0u8, 0u8, 1u8, 0u8, 1u8, 0u8, 10u8, 0u8, 16u8, 0u8, 1u8,
            //         |attributes_count|
            0u8, 0u8, //         | remaining
            42u8, 8u8,
        ];
        let parsed = attribute_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse Code_attribute: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            AttributeInfo {
                attribute_name_index: 1,
                attribute_length: 0,
                info: AttributeInfoTypes::Code {
                    max_stack: 10,
                    max_locals: 10,
                    code_length: 5,
                    code: vec![0, 108, 5, 10, 0],
                    exception_table_length: 1,
                    exception_table: vec![ExceptionData {
                        start_pc: 1,
                        end_pc: 10,
                        handler_pc: 16,
                        catch_type: 1
                    }],
                    attributes_count: 0,
                    attribute_info: Vec::new()
                }
            }
        );
        assert_eq!(
            remaining,
            [42u8, 8u8],
            "Parsed too much data for Code_attribute"
        );
    }

    #[test]
    fn attribute_info_parse_stack_map_table() {
        let cp = vec![ConstantInfo::Utf8Info {
            data: "StackMapTable".into(),
        }];
        //         | attribute_name_index | attribute_length  | number_of_entries | entries
        let data = [
            0u8, 1u8, 0u8, 0u8, 0u8, 0u8, 0u8, 2u8, 42u8, 42u8, //         | remaining
            42u8, 8u8,
        ];
        let parsed = attribute_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse StackMap_attribute: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            AttributeInfo {
                attribute_name_index: 1,
                attribute_length: 0,
                info: AttributeInfoTypes::StackMapTable {
                    number_of_entries: 2,
                    entries: vec![StackMapFrame::same_frame(42), StackMapFrame::same_frame(42)]
                }
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn attribute_info_exceptions_parse() {
        let cp = vec![ConstantInfo::Utf8Info {
            data: "Exceptions".into(),
        }];
        //         | attribute_name_index | attribute_length  | number_of_exceptions | exception_index_table
        let data = [
            0u8, 1u8, 0u8, 0u8, 0u8, 4u8, 0u8, 2u8, 0u8, 42u8, 0u8, 16u8,
            //         | remaining
            42u8, 8u8,
        ];
        let parsed = attribute_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse Exceptions_attribute: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            AttributeInfo {
                attribute_name_index: 1,
                attribute_length: 4,
                info: AttributeInfoTypes::Exceptions {
                    number_of_exceptions: 2,
                    exception_index_table: vec![42, 16]
                }
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn attribute_info_parse_bootstrap_methods() {
        let cp = vec![ConstantInfo::Utf8Info {
            data: "BootstrapMethods".into(),
        }];
        //         | attribute_name_index | attribute_length  | num_bootstrap_methods | bootstrap_methods
        let data = [
            0u8, 1u8, 0u8, 0u8, 0u8, 0u8, 0u8, 1u8, 0u8, 42u8, 0u8, 1u8, 0u8, 1u8,
            //         | remaining
            42u8, 8u8,
        ];
        let parsed = attribute_info_parse(&data, &cp);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse BootstrapMethods_attribute: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            AttributeInfo {
                attribute_name_index: 1,
                attribute_length: 0,
                info: AttributeInfoTypes::BootstrapMethods {
                    num_bootstrap_methods: 1,
                    bootstrap_methods: vec![BootstrapMethodsData {
                        bootstrap_method_ref: 42,
                        num_bootstrap_arguments: 1,
                        bootstrap_arguments: vec![1]
                    }]
                }
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    fn bootstrap_method_data_parser_test() {
        //        | bootstrap_method_ref | num_bootstrap_arguments | bootstrap_arguments |
        let data = [
            0u8, 1u8, 0u8, 2u8, 0u8, 10u8, 0u8, 15u8, //        | remaining
            42u8, 8u8,
        ];
        let parsed = bootstrap_method_data_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse bootstrap_method_data_parser: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            BootstrapMethodsData {
                bootstrap_method_ref: 1,
                num_bootstrap_arguments: 2,
                bootstrap_arguments: vec![10, 15]
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn exception_data_parse_test() {
        //         | start_pc |   end_pc   |  handler_pc | catch_type | remaining
        let data = [0u8, 10u8, 0u8, 25u8, 0u8, 10u8, 0u8, 0u8, 42u8, 8u8];
        let parsed = exception_data_parse(&data);
        assert!(
            parsed.is_ok(),
            format!("Was not able to parse exception_data: {:?}", parsed)
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            ExceptionData {
                start_pc: 10,
                end_pc: 25,
                handler_pc: 10,
                catch_type: 0
            }
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_same_frame() {
        //         | tag | remaining |
        let data = [42u8, 42u8, 8u8];
        let parsed = stack_map_frame_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse stack_map_frame::same_frame: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, StackMapFrame::same_frame(42));
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_same_locals_one_stack_item_frame() {
        //         | tag | tag | remaining |
        let data = [64u8, 0u8, 42u8, 8u8];
        let parsed = stack_map_frame_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse stack_map_frame::same_locals_one_stack_item_frame: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            StackMapFrame::same_locals_one_stack_item_frame(
                64,
                VerificationTypeInfo::top_variable_info()
            )
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_same_locals_one_stack_item_frame_extended() {
        //         | tag |  offset   | tag | remaining |
        let data = [247u8, 0u8, 10u8, 0u8, 42u8, 8u8];
        let parsed = stack_map_frame_parse(&data);
        assert!(parsed.is_ok(), format!("Was not able to parse stack_map_frame::same_locals_one_stack_item_frame_extended: {:?}", parsed));
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            StackMapFrame::same_locals_one_stack_item_frame_extended(
                247,
                10,
                VerificationTypeInfo::top_variable_info()
            )
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_chop_frame() {
        //         | tag |  offset  | remaining |
        let data = [248u8, 0u8, 10u8, 42u8, 8u8];
        let parsed = stack_map_frame_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse stack_map_frame::chop_frame: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, StackMapFrame::chop_frame(248, 10));
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_same_frame_extended() {
        //         | tag |   offset | remaining |
        let data = [251u8, 0u8, 10u8, 42u8, 8u8];
        let parsed = stack_map_frame_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse stack_map_frame::same_frame_extended: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, StackMapFrame::same_frame_extended(251, 10));
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_append_frame() {
        //         | tag |  offset   | locals | remaining |
        let data = [252u8, 0u8, 10u8, 1u8, 42u8, 8u8];
        let parsed = stack_map_frame_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse stack_map_frame::append_frame: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            StackMapFrame::append_frame(
                252,
                10,
                vec![VerificationTypeInfo::integer_variable_info()]
            )
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn stack_map_frame_parse_full_frame() {
        //         | tag |  offset   | number_of_locals | locals | number_of_stack_itens |  stack  | remaining |
        let data = [
            255u8, 0u8, 10u8, 0u8, 1u8, 0u8, 0u8, 2u8, 1u8, 2u8, 42u8, 8u8,
        ];
        let parsed = stack_map_frame_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse stack_map_frame::full_frame: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            StackMapFrame::full_frame(
                255,
                10,
                vec![VerificationTypeInfo::top_variable_info()],
                vec![
                    VerificationTypeInfo::integer_variable_info(),
                    VerificationTypeInfo::float_variable_info()
                ]
            )
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_parse_top_variable_info() {
        //         | tag | remaining |
        let data = [0u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::top_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::top_variable_info());
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_integer_variable_info() {
        //         | tag | remaining |
        let data = [1u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::integer_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::integer_variable_info());
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_float_variable_info() {
        //         | tag | remaining |
        let data = [2u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::float_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::float_variable_info());
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_long_variable_info() {
        //         | tag | remaining |
        let data = [3u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::long_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::long_variable_info());
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_double_variable_info() {
        //         | tag | remaining |
        let data = [4u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::double_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::double_variable_info());
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_null_variable_info() {
        //         | tag | remaining |
        let data = [5u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::null_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::null_variable_info());
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_unitialized_this_variable_info() {
        //         | tag | remaining |
        let data = [6u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(parsed.is_ok(), format!("Was not able to parse verification_type_info::unitialized_this_variable_info: {:?}", parsed));
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(
            parsed,
            VerificationTypeInfo::unitialized_this_variable_info()
        );
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_object_variable_info() {
        //         | tag | cp_index | remaining |
        let data = [7u8, 0u8, 10u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::object_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::object_variable_info(10));
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_unitialized_variable_info() {
        //         | tag | offset   | remaining |
        let data = [8u8, 0u8, 10u8, 42u8, 8u8];
        let parsed = verification_type_info_parse(&data);
        assert!(
            parsed.is_ok(),
            format!(
                "Was not able to parse verification_type_info::unitialized_variable_info: {:?}",
                parsed
            )
        );
        let (remaining, parsed) = parsed.unwrap();
        assert_eq!(parsed, VerificationTypeInfo::unitialized_variable_info(10));
        assert_eq!(remaining, [42u8, 8u8], "Parsed too much");
    }

    #[test]
    fn verification_type_info_error_parse() {
        //TODO
    }

}
