extern crate clap;

use clap::{App, Arg};

pub enum Execution {
    Jar(String),
    Classfile(String),
}

struct Application {
    exe: Execution,
    arguments: Vec<String>,
    classpath: Option<String>,
    properties: Vec<String>,
    enable_assertions: bool,
}

fn main() {
    let matches = App::new("JustVM")
        .version("0.1")
        .author("Pedro Jordão <pedrohjordao@gmail.com>")
        .about("Just a JVM in Rust")
        .arg(
            Arg::with_name("verbose")
                .possible_values(&["gc", "class", "jni"])
                .help("Enables verbose printing")
                .multiple(true),
        )
        .arg(
            Arg::with_name("enableassertions")
                .short("ea")
                .help("Enables assertions for everything"),
        )
        .arg(
            Arg::with_name("classpath")
                .short("cp")
                .help("A list separated by ; with the paths to JAR files"),
        )
        .arg(
            Arg::with_name("property")
                .short("D")
                .help("A system property defined as <property_name>=<property_value>")
                .multiple(true),
        )
        .arg(
            Arg::with_name("classfile")
                .short("c")
                .conflicts_with("jar")
                .help("Runs a class")
                .required_unless("jar"),
        )
        .arg(
            Arg::with_name("jar")
                .short("jar")
                .conflicts_with("classfile")
                .help("Runs a JAR file")
                .required_unless("classfile"),
        )
        .get_matches();

    let execution_and_args = match (matches.value_of("jar"), matches.value_of("classfile")) {
        (Some(_), _) => {
            println!("Right now JustVM does not support jar loading");
            return;
        }
        (_, Some(classfile_and_args)) => {
            let split = split_filename_from_app_args(classfile_and_args);
            (Execution::Classfile(split.0), split.1)
        }
        _ => unreachable!(),
    };

    let classpath = matches.value_of("classpath").map(|x| String::from(x));
    let enable_assertions = matches.value_of("enableassertions").is_some();
    let vm_options = matches
        .values_of("properties")
        .map(|values| values.map(|x| String::from(x)).collect::<Vec<_>>())
        .unwrap_or_default();

    start_jvm(Application {
        exe: execution_and_args.0,
        arguments: execution_and_args.1,
        classpath,
        enable_assertions,
        properties: vm_options,
    });
}

fn split_filename_from_app_args(text: &str) -> (String, Vec<String>) {
    let mut vec_string: Vec<String> = text.split_whitespace().map(|x| x.into()).collect();

    let args = vec_string.split_off(0);
    (vec_string.remove(0), args)
}

fn start_jvm(app: Application) {
    unimplemented!()
}
